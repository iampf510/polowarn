#!/usr/bin/env python2
#!coding:utf-8
import time
import json
import os
import urllib2
import sqlite3
import datetime


def connectDB():
    if os.path.exists('polo.db'):
        return sqlite3.connect('polo.db')
    else:

        conn = sqlite3.connect('polo.db')
        conn.execute('''CREATE TABLE POLO
            (pair CHAR(20),
            price REAL,
            volume REAL,
            bid REAL,
            ask REAL,
            time INT);'''),
        return conn

def insertDB(conn, data):
    for d in data:
        values = (d['pair'], d['price'], d['volume'], d['bid'], d['ask'], d['time'])
        cmd = 'INSERT INTO POLO (pair, price, volume, bid, ask, time) VALUES (\'%s\', %f, %f, %f, %f, %d);' % values
        conn.execute(cmd)
    conn.commit()

def getTicker():
    return json.loads(urllib2.urlopen('https://poloniex.com/public?command=returnTicker').read())

def getPairs():
    temp_d = getTicker()
    data = []
    for pair in temp_d:
        if pair.startswith('BTC'):      #只觀察跟btc有關的貨幣
            p = {}
            p['pair'] = pair
            p['volume'] = float(temp_d[pair]['baseVolume'])
            data.append(p)
    p = pd.DataFrame(data)
    return p.sort_values('volume', ascending=0)['pair'].tolist()[:20]

def getPairData(timestamp):
    temp_d = getTicker()
    data = []
    for pair in temp_d:
        if pair.startswith('BTC') or pair.startswith('USDT'):      #只觀察跟btc還有usdt有關的貨幣
            p = {}
            p['pair'] = pair
            p['price'] = float(temp_d[pair]['last'])
            p['volume'] = float(temp_d[pair]['baseVolume'])
            p['time'] = timestamp
            p['bid'] = float(temp_d[pair]['highestBid'])
            p['ask'] = float(temp_d[pair]['lowestAsk'])
            data.append(p)
    return data



if __name__ == '__main__':

    conn = connectDB()
    err_log = open('error_log', 'a')
    while 1:
        time.sleep(2)
        timestamp = int(time.time())
        try:
            data = getPairData(timestamp)
            insertDB(conn, data)
        except KeyboardInterrupt:
            conn.close()
            exit()

        except Exception, e:
            err_log.write(str(datetime.datetime.now()) + '\t' + str(e) + '\n')
            pass




		
