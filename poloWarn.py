#!/usr/bin/env python2
#!coding:utf-8
import time
import json
import urllib2
import pandas as pd
import pynotify

def warning(msg):
    pynotify.Notification('Poloniex', msg).show()


def getTicker():
    return json.loads(urllib2.urlopen('https://poloniex.com/public?command=returnTicker').read())

def getPairs():
    temp_d = getTicker()
    data = []
    for pair in temp_d:
        if pair.startswith('BTC'):      #只觀察跟btc有關的貨幣
            p = {}
            p['pair'] = pair
            p['volume'] = float(temp_d[pair]['baseVolume'])
            data.append(p)
    p = pd.DataFrame(data)
    return p.sort_values('volume', ascending=0)['pair'].tolist()[:20]

def getPairData(timestamp):
    temp_d = getTicker()
    data = []
    for pair in temp_d:
        if pair.startswith('BTC'):      #只觀察跟btc有關的貨幣
            p = {}
            p['pair'] = pair
            p['price'] = float(temp_d[pair]['last'])
            p['time'] = timestamp
            data.append(p)
    return data


def checkChance(p, pairs, timestamp):
    p_1_min = p[p['time'] >=timestamp-60][p['time']<timestamp]  #取一分鐘之內的data
    p_now = p[p['time'] == timestamp]                           #現價

    msg = []

    for pair in pairs:
        #max_1_min = p_1_min[p_1_min['pair'] == pair]['price'].max()     #1分鐘內的最大價
        #min_1_min = p_1_min[p_1_min['pair'] == pair]['price'].min()     #1分鐘內的最小價
        price_1_min = p_1_min[p_1_min['pair'] == pair]['price'].tolist()[0] #1分鐘前現價
        price_now = p_now[p['pair'] == pair]['price'].tolist()[0]          #現價
        #if price_now / min_1_min >= 1.01 or price_now / min_1_min <= 0.99 or price_now / max_1_min >= 1.01 or price_now / max_1_min <= 0.99:
        if price_now / price_1_min >= 1.02 or price_now / price_1_min <= 0.98:
            print pair.ljust(10), '%.8f  %.8f  %.3f' % (price_1_min, price_now, (price_now / price_1_min-1)*100)
            msg.append('%10s  %.8f  %.8f  %.3f' % (pair, price_1_min, price_now, (price_now/price_1_min-1)*100))
    if len(msg) != 0:
        warning('\r'.join(msg))



if __name__ == '__main__':
    pynotify.init('Poloniex Warning')
    timestamp = int(time.time())
    pairs = getPairs()

    p = pd.DataFrame(getPairData(timestamp))
    d = []
    i = 0
    while True:
        time.sleep(5)
        timestamp = int(time.time())
        x = pd.DataFrame(getPairData(timestamp))
        p = p.append(x, ignore_index=True)
        p = p[p['time'] > timestamp-60*30]

        checkChance(p, pairs, timestamp)
        
        i += 1
        if i % 100 == 0:
            pairs = getPairs()


		
